var Header = React.createClass({
    render: function () {
        return (
            <header className="bar bar-nav">
                <a href="#" className={"icon icon-left-nav pull-left" + (this.props.back==="true"?"":" hidden")}></a>
                <h1 className="title">{this.props.text}</h1>
            </header>
        );
    }
});

var SearchBar = React.createClass({
    searchHandler: function() {
        this.props.searchHandler(this.refs.searchKey.getDOMNode().value);
    },
    render: function () {
        return (
            <div className="bar bar-standard bar-header-secondary">
                <input type="search" ref="searchKey" onChange={this.searchHandler} value={this.props.searchKey}/>
            </div>

        );
    }
});

var ContactListItem = React.createClass({
    render: function () {
        return (
            <li className="table-view-cell media">
                <a href={"#contacts/" + this.props.contact.id}>
                    <img className="media-object small pull-left" src={"pics/person.jpg" }/>
                    {this.props.contact.firstName} {this.props.contact.lastName}
                    <p>{this.props.contact.title}</p>
                </a>
            </li>
        );
    }
});

var EmployeeList = React.createClass({
    render: function () {
        var items = this.props.contacts.map(function (contact) {
            return (
                <ContactListItem key={contact.id} contact={contact} />
            );
        });
        return (
            <ul  className="table-view">
                {items}
            </ul>
        );
    }
});

var HomePage = React.createClass({
    render: function () {
        return (
            <div className={"page " + this.props.position}>
                <Header text="COntacts" back="false"/>
                <SearchBar searchKey={this.props.searchKey} searchHandler={this.props.searchHandler}/>
                <div className="content">
                    <EmployeeList contacts={this.props.contacts}/>
                </div>
            </div>
        );
    }
});

var EmployeePage = React.createClass({
    getInitialState: function() {
        return {contact: {}};
    },
    componentDidMount: function() {
        this.props.service.findById(this.props.contactId).done(function(result) {
            this.setState({contact: result});
        }.bind(this));
    },
    render: function () {
        return (
            <div className={"page " + this.props.position}>
                <Header text="Contact" back="true"/>
                <div className="card">
                    <ul className="table-view">
                        <li className="table-view-cell media">
                            <img className="media-object big pull-left" src={"pics/person.jpg" }/>
                            <h1>{this.state.contact.firstName} {this.state.contact.lastName}</h1>
                            <p>{this.state.contact.title}</p>
                        </li>
                        <li className="table-view-cell media">
                            <a href={"tel:" + this.state.contact.officePhone} className="push-right">
                                <span className="media-object pull-left icon icon-call"></span>
                                <div className="media-body">
                                Call Office
                                    <p>{this.state.contact.officePhone}</p>
                                </div>
                            </a>
                        </li>
                        <li className="table-view-cell media">
                            <a href={"tel:" + this.state.contact.mobilePhone} className="push-right">
                                <span className="media-object pull-left icon icon-call"></span>
                                <div className="media-body">
                                Call Mobile
                                    <p>{this.state.contact.mobilePhone}</p>
                                </div>
                            </a>
                        </li>
                        <li className="table-view-cell media">
                            <a href={"sms:" + this.state.contact.mobilePhone} className="push-right">
                                <span className="media-object pull-left icon icon-sms"></span>
                                <div className="media-body">
                                SMS
                                    <p>{this.state.contact.mobilePhone}</p>
                                </div>
                            </a>
                        </li>
                        <li className="table-view-cell media">
                            <a href={"mailto:" + this.state.contact.email} className="push-right">
                                <span className="media-object pull-left icon icon-email"></span>
                                <div className="media-body">
                                Email
                                    <p>{this.state.contact.email}</p>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
});

var App = React.createClass({
    mixins: [PageSlider],
    getInitialState: function() {
        return {
            searchKey: '',
            contacts: []
        }
    },
    searchHandler: function(searchKey) {
        contactService.findByName(searchKey).done(function(contacts) {
            this.setState({
                searchKey:searchKey,
                contacts: contacts,
                pages: [<HomePage key="list" searchHandler={this.searchHandler} searchKey={searchKey} contacts={contacts}/>]});
        }.bind(this));
    },
    componentDidMount: function() {
        router.addRoute('', function() {
            this.slidePage(<HomePage key="list" searchHandler={this.searchHandler} searchKey={this.state.searchKey} contacts={this.state.contacts}/>);
        }.bind(this));
        router.addRoute('contacts/:id', function(id) {
            this.slidePage(<EmployeePage key="details" contactId={id} service={contactService}/>);
        }.bind(this));
        router.start();
    }
});

React.render(<App/>, document.body);