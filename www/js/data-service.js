contactService = (function () {

    var baseURL = "";

    // The public API
    return {
        findById: function(id) {
            return $.ajax(baseURL + "/contacts/" + id);
        },
        findByName: function(searchKey) {
            return $.ajax({url: baseURL + "/contacts", data: {name: searchKey}});
        }
    };

}());