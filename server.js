const PORT = 2155;
const express = require('express');
const app = express();
const path = require('path');
const cassandra = require('cassandra-driver');
const async = require('async');
const bodyParser = require('body-parser');
const cors = require('cors')
const _ = require('lodash');


app.use(cors());
app.use(bodyParser.json());


app.listen(PORT, function ()
{
  console.log('service port ' + PORT + '!');
});


const KEYSPACE = 'demo';

function connectToCassandra(keyspace)
{
	return new Promise((success, failure)=>
	{
		var clientParams;
		if(_.isNil(keyspace))
		{
			clientParams = { contactPoints: ['127.0.0.1']};
		}
		else
		{
			clientParams = { contactPoints: ['127.0.0.1'], keyspace: keyspace};
		}
		const client = new cassandra.Client(clientParams);
		client.connect(function(connectError)
		{
			console.log("Connected. Executing query...");
			if(connectError)
			{
				console.log("connectError:", connectError);
				return failure(connectError);
			}
			success(client);
		});
	});	
}

app.get('/create', function(req, res)
{
	console.log("Connecting to Cassandra...");
	connectToCassandra()
	.then((client)=>
	{
		console.log("Connected. Executing query...");
		const query = "CREATE KEYSPACE " + KEYSPACE + " WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 }";
		client.execute(query, function(err, result) {
			if(err)
			{
				console.log("err:", err);
				res.send(err);
				return;
			}
			console.log("create result:", result);
			res.send(result);
		});
	})
	.catch((error)=>
	{
		res.send(error);
	});
});

app.get('/createtables', function(req, res)
{
	connectToCassandra(KEYSPACE)
	.then((client)=>
	{
		const query = 'CREATE TABLE contacts (id int PRIMARY KEY,firstName text,lastName text,title text,mobilePhone text,officePhone text,email text,city text)';
		client.execute(query, function(err, result)
		{
			if(err)
			{
				console.log("err:", err);
				res.send(err);
				return;
			}
			console.log("create table result:", result);
			res.send(result);
		});
	})
	.catch((error)=>
	{
		res.send(error);
	});
});

app.post('/createcontact', function(req, res)
{
	var contact = req.body;
	console.log("contact:", contact);
	connectToCassandra(KEYSPACE)
	.then((client)=>
	{
		var query = "INSERT INTO contacts (user_id, firstname, lastname, company, phone) VALUES (";
		query += contact.id + ", '" + contact.firstName + "', '" + contact.lastName + "',";
		query += " '" + contact.company + "', '" + contact.homeNumber + "')";
		console.log("query:", query);
		client.execute(query, function(err, result)
		{
			if(err)
			{
				console.log("err:", err);
				res.send(err);
				return;
			}
			console.log("insert contact result:", result);
			res.send({result: true, data: result});
		});
	})
	.catch((error)=>
	{
		res.send({result: false, error: error});
	});
});

app.get('/contacts/all', function(req, res)
{
	connectToCassandra(KEYSPACE)
	.then((client)=>
	{
		const query = "SELECT * FROM contacts";
		client.execute(query, function(err, result)
		{
			if(err)
			{
				console.log("err:", err);
				res.send(err);
				return;
			}
			console.log("get all contacts:", result);
			result.rows = _.map(result.rows, (item)=>
			{
				return {
					id: item.id,
					firstName: item.firstname,
					lastName: item.lastname,
					title: item.title,
					mobilePhone: item.mobilePhone
				};
			});
			res.send({result: true, data: result.rows});
		});
	})
	.catch((error)=>
	{
		res.send({result: false, error: error});
	});
});



// Generic error handler
function handleError(res, reason, message, code) {
  console.log("ERROR: " + reason);
  res.status(code || 500).json({"error": message});
}


/*  "/contacts"
 *    GET: get all contacts
 *    POST: create new contact
 */

app.get("/contacts", function(req, res) { 
  client.execute("SELECT id,firstName,lastName,title,mobilePhone,officePhone,email,city FROM contacts", function (err, result) {
           if (!err){
               if ( result.rows.length > 0 ) {
                   var user = result.rows[0];
                   console.log("name = %s, id = %d", user.firstname, user._id);
				   res.status(200).json(result);
               } else {
                   console.log("No results");
               }
           }
           else{
				// Run next function in series
				handleError(res, err.message, "Failed to get contacts.");
		   }
       });
});


app.post("/contacts", function(req, res) {

	const query = 'INSERT INTO contacts (id,firstName,lastName,title,mobilePhone,officePhone,email,city) VALUES (?, ?, ?, ?, ?, ?, ?,?)';
	const params = [req.params.id, req.params.firstName, req.params.lastname,req.params.title, req.params.mobile, req.params.workphone, req.params.email,req.params.city ];
	client.execute(query, params, { prepare: true }, function (err, res) {
	  if (err) {
			handleError(res, err.message, "Failed to create new contact.");
		} else {
		  res.status(201).json(res);
		}
	});
  
});

/*  "/contacts/:id"
 *    GET: get contacts by id
 *    PUT: update contacts by id
 *    DELETE: deletes contacts by id
 */

app.get("/contacts/:id", function(req, res) {

    client.execute("SELECT id,firstName,lastName,title,mobilePhone,officePhone,email,city FROM contacts WHERE id = req.params.id", function (err, result) {
           if (!err){
               if ( result.rows.length > 0 ) {
                   var user = result.rows[0];
                   console.log("name = %s, id = %d", user.firstname, user._id);
				   res.status(200).json(result);
               } else {
                   console.log("No results");
               }
           }
           else{
				// Run next function in series
				handleError(res, err.message, "Failed to get contact.");
		   }
       });
});

app.put("/contacts/:id", function(req, res) {

      // Update contact info
       client.execute("UPDATE contacts SET id = req.params.id,firstName=req.params.firstName,lastname=req.params.lastname,title=req.params.title,city=req.params.city, email=req.params.email, mobile=req.params.mobile,officePhone=req.params.workphone WHERE _id = req.params.id", function (err, result) {
           // Run next function in series
            if (err) {
				handleError(res, err.message, "Failed to update contact");
			} else {
			  res.status(203).end();
			}
       });
   
});

app.delete("/contacts/:id", function(req, res) {

      client.execute("DELETE FROM contacts WHERE id = req.params.id", function (err, result) {
           if (!err){
				   res.status(204).json(result);
           }
           else{
				// Run next function in series
				handleError(res, err.message, "Failed to get contact.");
		   }
       });
  
});
